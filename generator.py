from reportlab.graphics.barcode import code128
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm, inch
from reportlab.pdfgen import canvas
from reportlab.graphics.barcode import createBarcodeDrawing
from reportlab.graphics.shapes import Drawing, Image
from reportlab.graphics import renderPDF

labels_horizontal = 5
labels_vertical = 13

page_width = 210 * mm
page_height = 297 * mm
margin_left = 10 * mm
margin_right = 10 * mm
margin_top = 10 * mm
margin_bottom = 10 * mm
label_width = (page_width - margin_left - margin_right) / labels_horizontal
label_height = (page_height - margin_top - margin_bottom) / labels_vertical
labels_per_page = labels_horizontal * labels_vertical
serial_no_format = "{}-{}"
serial_no_length = 6
barcode_width = 40 * mm

logo_filename = './logo.jpg'

def get_barcode(value, width, barWidth = 1 * mm, fontSize = 35, humanReadable = True):

    barcode = createBarcodeDrawing('Code128', value = value, barWidth = barWidth, fontSize = fontSize, humanReadable = humanReadable)

    drawing_width = width
    barcode_scale = drawing_width / barcode.width
    drawing_height = barcode.height * barcode_scale

    drawing = Drawing(drawing_width, drawing_height)
    drawing.scale(barcode_scale, barcode_scale)
    drawing.add(barcode, name='barcode')

    return drawing

def get_serial_numbers(device_code, begin_no, count):
    serial_numbers = []
    for sn in range(begin_no, begin_no + count):
        serial_no = serial_no_format.format(device_code, str(sn).zfill(serial_no_length))
        serial_numbers.append(serial_no)

    return serial_numbers

def createBarCodes(barcode_values):
    c = canvas.Canvas("barcodes.pdf", pagesize=A4)

    logo = Image(0, 0, barcode_width, 8 * mm, logo_filename)
    logo_d = Drawing(0, 0)
    logo_d.add(logo)
    logo_height = logo_d.height
    margin = 0 * mm

    label_no = 0
    for barcode_value in barcode_values:
        d = get_barcode(barcode_value, barcode_width)

        x_no = (label_no % labels_horizontal)
        y_no = int(label_no / labels_horizontal)

        x_pos = margin_left + (label_width * x_no) + (label_width / 2) - (d.width / 2)

        h = logo_height + margin + d.height

        y_pos = page_height - margin_top - (label_height * y_no) - (label_height / 2) - (h / 2)
        renderPDF.draw(d, c, x_pos, y_pos)

        y_pos += (d.height + margin)

        renderPDF.draw(logo_d, c, x_pos, y_pos + logo_d.height)

        label_no += 1

        if (label_no >= labels_per_page):
            c.showPage()
            label_no = 0

    c.save()

if __name__ == "__main__":
    # barcode_values = get_serial_numbers("RSTX01", 204010, 10)
    # barcode_values.extend(get_serial_numbers("RTS01", 61, 50))
    # barcode_values = get_serial_numbers("RTS01", 111, 65)

    createBarCodes(barcode_values)